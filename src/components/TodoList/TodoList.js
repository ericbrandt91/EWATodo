import React, {Component} from 'react';
import TodoItem from  './TodoItem';
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux';
import {detailTodo} from './action';

class TodoList extends Component {
    
    render() {
        var detailView = this.props.detailView;
        return (
            <div>
            <div className="col-sm-6">
            <button className="btn btn-success">SAVE</button>
               {this.props.todos.map((todo)=>{
                   return <TodoItem key={todo.id} todo={todo}/>
               })}
            Details: <input checked={detailView} type='checkbox' onChange={() => this.props.checkBox({detailView})}></input> 
            </div>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        todos: state.todos.todos,
        detailView: state.todos.detailView
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators({checkBox:detailTodo},dispatch)

}
    
export default connect(mapStateToProps, mapDispatchToProps)(TodoList);
