import 'babel-polyfill'
import ReactDOM from 'react-dom'
import React from 'react'
import Layout from './components/Layout/Layout'
import {createStore} from 'redux'
import allReducers from './reducers'
import {Provider} from 'react-redux'


const store = createStore(allReducers);

ReactDOM.render(    
    <Provider store={store}>
        <Layout />
    </Provider>,document.getElementById('root')
);