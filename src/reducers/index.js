import {combineReducers} from 'redux'
import todoReducer from '../components/TodoList/todo-reducer'
const allReducers = combineReducers({
    todos:todoReducer
})

export default allReducers