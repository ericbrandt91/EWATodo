export const deleteTodo = (todo) => {
    console.log("New click on: "+todo.id);
    return {
        type:"DELETE_TODO",
        payload:todo
    }
};

export const editTodo = (todo) => {
    console.log("New click on: "+todo.id);
    return {
        type:"EDIT_TODO",
        payload:todo
    }
};

export const detailTodo = (detailView) => { 
    let isChecked = detailView.detailView;   
    console.log("detail clicked: " + isChecked);   
    if(isChecked){
        isChecked = false;
    }else{
        isChecked = true;
    }
    console.log(isChecked);
    return {
        type:"DETAIL_TODO",
        payload:isChecked
    }
};

export const editChanged = (event,todo) => {
var newTodo = {"id":todo.id,"title":event.target.value,"detail":todo.detail}
   return {
       type:"INPUT_CHANGED",
       payload:newTodo
   }
}

export const detailChanged = (event,todo) => {
var newTodo = {"id":todo.id,"title":todo.title,"detail":event.target.value}
   return {
       type:"INPUT_DETAIL_CHANGED",
       payload:newTodo
   }
}

export const saveEdited = (todo) => {

   return {
       type:"SAVE_EDITED",
       payload:todo
   }
}
export const addDetail = (event,todo) => {
var newTodo = {"id":1,"title":todo.title,"detail":event.target.value}
   return {
       type:"ADD_TITLE",
       payload:newTodo
  }   
}
export const addTitle = (event,todo) => {
var newTodo = {"id":1,"title":event.target.value,"detail":todo.detail}
   return {
       type:"ADD_TITLE",
       payload:newTodo
   }
}

export const addTodo = (todo) => {
   return {
       type:"ADD_TODO",
       payload:todo
   }
}

