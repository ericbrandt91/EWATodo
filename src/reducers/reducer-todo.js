export default function() {

    return [{
        "id":1,
        "title":"Das ist der Titel der ersten Aufgabe!",
        "detail":"Detailaufgabe"
    },
    {
        "id":2,
        "title":"Das ist der Titel der zweiten Aufgabe!",
        "detail":"Detail der zweiten Aufgabe"
    }]
}