import React, {Component} from 'react';
import {editChanged,detailChanged,saveEdited} from './action';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

class EditTodo extends Component {
    render() {
        let {id,title,detail} = this.props.todo;
        return (
      
            <div className="col-sm-6 list-group">
               
                <h1 className="list-group-item">Edit Todo Nr.: {id}</h1>
                 <div className="list-group-item">
                <p>Titel: {title}</p>
                <p>Detail: {detail}</p>
                <h1>Gib deine Änderungen an</h1>
                <div className="input-group">
                    <span className="input-group-addon">Titel </span>
                    <input className="form-control"  placeholder="Gib einen Titel ein..." value={title} onChange={(e) => this.props.editChanged(e,this.props.todo)}></input>
                </div>
                <br/>
                <div className="input-group">
                    <span className="input-group-addon">Detail </span>
                    <input className="form-control"  placeholder="Gib Details ein..." value={detail} onChange={(e) => this.props.detailChanged(e,this.props.todo)}></input>
                </div>
                <button onClick={() => {this.props.saveEdited(this.props.todo)}}>SAVE</button>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        todo: state.todos.edit
    }
}
const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators({
                                editChanged:editChanged,
                                detailChanged:detailChanged,
                                saveEdited:saveEdited
                              },
                              dispatch)
}
    
export default connect(mapStateToProps,mapDispatchToProps)(EditTodo);
