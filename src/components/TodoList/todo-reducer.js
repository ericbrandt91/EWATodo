import EditTodo from './EditTodo';

const INITIAL_STATE = {todos: [{
        "id":1,
        "title":"Das ist der Titel der ersten Aufgabe!",
        "detail":"Detailaufgabe"
    },
    {
        "id":2,
        "title":"Das ist der Titel der zweiten Aufgabe!",
        "detail":"Detail der zweiten Aufgabe"
    }],detailView:false,newtodo:{id:"",title:"",detail:""}};

export default function(state=INITIAL_STATE,action) {
     let newState = null;
    switch (action.type) {       
        case 'DELETE_TODO':
             //console.log("Delete clicked: " + JSON.stringify(action.payload));
             var {todos} = state;
             var newtodos = todos.filter((x) => {return x.id != action.payload.id})
             //console.log("INDEX: "+ index + " TODOS: "+ JSON.stringify(todos))
             return Object.assign({},state,{todos:newtodos,edit:null});           
        case 'EDIT_TODO':
             newState = Object.assign({},state,{"edit":action.payload})
             console.log("State: "+ JSON.stringify(newState));
             return newState;
        case 'DETAIL_TODO':
             console.log("DETAIL_TODO: " + action.payload)
             newState =  Object.assign({},state,{"detailView":action.payload})
             //console.log("Detailstate: "+ JSON.stringify(newState));
             return newState;
        case 'INPUT_CHANGED':            
            var newTodo = action.payload;
            newState = Object.assign({},state,{"edit":newTodo})
            //var ntodos = state.todos.map((todo)=>{if(newTodo.id === todo.id) todo = newTodo;return todo});            
            //newState = Object.assign({},state,{"todos":ntodos,edit:newTodo})     
            //console.log("newState: "+ JSON.stringify(newState))       
            return newState;               
        case 'INPUT_DETAIL_CHANGED':
            var newTodo = action.payload;
            newState = Object.assign({},state,{"edit":newTodo})
            //var ntodos = state.todos.map((todo)=>{if(newTodo.id === todo.id) todo = newTodo;return todo});            
            //newState = Object.assign({},state,{"todos":ntodos,edit:newTodo})     
           // console.log("newState: "+ JSON.stringify(newState))       
            return newState;  
        case 'SAVE_EDITED' :
             var newTodo = action.payload;
             var ntodos = state.todos.map((todo)=>{if(newTodo.id === todo.id) todo = newTodo;return todo});            
             newState = Object.assign({},state,{"todos":ntodos,edit:null})
             return newState;  
        case 'ADD_DETAIL':
             var newTodo = action.payload;
             newState = Object.assign({},state,{"newtodo":newTodo})      
             return newState;       
        case 'ADD_TITLE' :
             var newTodo = action.payload;
             newState = Object.assign({},state,{"newtodo":newTodo})      
             return newState;   
        case 'ADD_TODO'  :
            
             console.log("ADD_TODO: " + JSON.stringify(state.todos))
             var newTodo = action.payload;  
             if(state.todos.length < 1) maxID = 1
             else 
             {
                 var maxID = state.todos.map((x)=>{return x.id}).reduce((a,b)=>{return Math.max(a,b)})
                newTodo.id = maxID + 1;
             }              
             var ntodos = [...state.todos,newTodo];
             newState = Object.assign({},state,{todos:ntodos,"newtodo":{id:null,title:"",detail:""}})  
             
             return newState;
        default:
    return state;
    }    
}