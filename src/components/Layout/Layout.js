import React, {Component} from 'react';
import TodoList from '../TodoList/TodoList'
import AddTodo from '../TodoList/AddTodo'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import EditTodo from '../TodoList/EditTodo'
//Layout Component of the WebShop
//This Shop will consist of three main components 
/*
*   navigationbar
*   shop list
*   footer   
*
*/

class Layout extends Component {
    render() {
        return (
            <div>
                <h1 className="jumbotron">TODO List </h1>
                <div className="row">
                    <TodoList />
                    {this.props.edit ? <EditTodo /> : <AddTodo />}
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        edit: state.todos.edit
    }
}

export default connect(mapStateToProps)(Layout);