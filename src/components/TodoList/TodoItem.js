import React, {Component} from 'react';
import {deleteTodo,editTodo} from './action.js'
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

class TodoItem extends Component {    
    render() {   
        var {id,title,detail} = this.props.todo; 
        return (
            <div className="list-group-item">
                <p className=""> Titel: {title} 
                    {this.props.detail ? <p>Detail: {detail}</p> : null}
                    <p><button className="btn btn-info" onClick={() => this.props.editTodo(this.props.todo)}>Edit</button>
                    <button className="btn btn-danger" onClick={() => this.props.deleteTodo(this.props.todo)}>X</button></p>
                </p>
            </div>
        );
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({deleteTodo: deleteTodo, editTodo: editTodo},dispatch)
}

const mapStateToProps = (state, ownProps) => {
    return {
        detail: state.todos.detailView
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(TodoItem);