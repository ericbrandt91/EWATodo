import React, {Component} from 'react';
import {addTodo,addDetail,addTitle} from './action';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
class AddTodo extends Component {
    render() {
        let {id,title,detail} = this.props.todo;
        return (            
            <div className="col-sm-6">               
                <h1>Adde deine Aufgabe</h1>
                <div className="input-group">
                <span className="input-group-addon">Titel </span>
                <input className="form-control" placeholder="Gib einen Titel ein..." value={title} onChange={(e) => this.props.addTitle(e,this.props.todo)}></input>
                 </div>
                 <br/>
                 <div className="input-group">
                     <span className="input-group-addon">Detail</span>
                     <input className="form-control"  placeholder="Gib Details ein..." value={detail} onChange={(e) => this.props.addDetail(e,this.props.todo)}></input>
                </div>
                <button className="btn btn-success" onClick={() => {this.props.addTodo(this.props.todo)}}>SAVE</button>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        todo: state.todos.newtodo
    }
}
const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators({
                                addTodo:addTodo,
                                addDetail:addDetail,
                                addTitle:addTitle
                              },
                              dispatch)
}
    
export default connect(mapStateToProps,mapDispatchToProps)(AddTodo);
